# To Run This App you need to install the packages
nvm use v12.13.1 # if nvm is present or else use the node version v12.13.1
npm install
to install all the packages 

TODO

- Scroll Modal Content
- Sorting by Last Opened

Done

- Search Books by name
- List View Sorting using Read Percentage
- Theming using toggle button
- Responsive
- Search Text UI
- Text Color using Theming for some components
- Sorting using Book Title, Genre and Percentage.

To start the server
  npm run start


# books-react
