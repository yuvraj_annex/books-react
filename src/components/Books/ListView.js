import React, { useContext, useCallback, useState } from "react";
import { BooksContext } from "../../context/BooksContext";

import "./ListView.css";
import { FiChevronDown } from "react-icons/fi";
import { BsArrowUpDown } from "react-icons/bs";
import styled from "styled-components";
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime)

const ListViewWrapper = styled.div`
  margin: 30px;
  @media (max-width: 768px) {
    margin: 0;
  }
`;
const ListView = () => {
  const { search, setSearch } = useContext(BooksContext);
  const [sortBooksData, setSort] = useState(null);
  // TimeAgo.addDefaultLocale(en);

  const sortBooks = useCallback(
    (type) => {
      if (sortBooksData === type) {
        let reSortBooks = search.sort((a, b) => {
          return a.id - b.id;
        });
        setSearch(reSortBooks);
        setSort(null);
      } else {
        let sortedBooks;
        if (type === "perc") {
          sortedBooks = search.sort((a, b) => {
            return a.read_percentage - b.read_percentage;
          });
        } else if (type === "author") {
          sortedBooks = search.sort((a, b) => {
            return ("" + a.title).localeCompare(b.title);
          });
        } else if (type === "genre") {
          sortedBooks = search.sort((a, b) => {
            return ("" + a.genre).localeCompare(b.genre);
          });
        } else if (type === "last") {
          sortedBooks = search.sort((a, b) => {
            return ("" + b.last_opened.toString()).localeCompare(a.last_opened.toString());
          });
        }
        setSearch(sortedBooks);
        setSort(type);
      }
    },
    [search, setSearch, sortBooksData]
  );

  return (
    <ListViewWrapper>
      <section>
        <header>
          <SortHeader
            className="col first mobile__header"
            onClick={() => sortBooks("author")}
          >
            Book Title & Author
            <span className={"sorting-icon"}>
              {sortBooksData === "author" ? <FiChevronDown /> : <BsArrowUpDown /> }
            </span>
          </SortHeader>
          <SortHeader className="col mobile" onClick={() => sortBooks("genre")}>
            Genre
            <span className={"sorting-icon"}>
              {sortBooksData === "genre" ? <FiChevronDown /> : <BsArrowUpDown /> }
            </span>
          </SortHeader>
          <SortHeader className="col mobile__header" onClick={() => sortBooks("perc")}>
            Reading Progress
            <span className={"sorting-icon"}>
              {sortBooksData === "perc" ? <FiChevronDown /> : <BsArrowUpDown /> }
            </span>
          </SortHeader>
          <SortHeader className="col mobile" onClick={() => sortBooks("last")}>
            Last Opened
            <span className={"sorting-icon"}>
              {sortBooksData === "last" ? <FiChevronDown /> : <BsArrowUpDown /> }
            </span>
          </SortHeader>
        </header>

        {search.map((book, index) => {
          return (
            <BookData
              genre={book.genre}
              title={book.title}
              last_opened={book.last_opened}
              read_percentage={book.read_percentage}
              image={book.image}
              author={book.author}
              description={book.description}
              key={index}
            />
          );
        })}
      </section>
    </ListViewWrapper>
  );
};
const BookData = ({
  author,
  title,
  image,
  genre,
  read_percentage,
  last_opened,
  description,
}) => {
  const { modal, setModal, setBookInfo } = useContext(BooksContext);
  const x = {
    genre,
    read_percentage,
    image,
    author,
    description,
    title,
  };
  return (
    <div
      className="row"
      onClick={() => {
        setModal(!modal);
        setBookInfo(x);
      }}
    >
      <div className="col first mobile__header">
        <img
          src={image}
          alt="da"
          height="100"
          style={{ borderRadius: "10px" }}
        />
        <div className="second">
          <div style={{ width: "70%", fontWeight: "bold" }}>
            <Heading>{title}</Heading>
          </div>
          <div>
            <SubHeading>{author}</SubHeading>
          </div>
        </div>
      </div>
      <SortHeader className="col mobile">{genre}</SortHeader>
      <SortHeader className="col mobile__header perc-header">{read_percentage}%</SortHeader>
      <SortHeader className="col mobile">{dayjs().to(dayjs(last_opened))}</SortHeader>
    </div>
  );
};
export default ListView;

const Heading = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  color: ${({ theme }) => theme.headingTextColor};;
  line-height: 24px;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;
const SubHeading = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;
  color: ${({ theme }) => theme.subheadingTextColor};
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;
const SortHeader = styled.div`
  color: ${({ theme }) => theme.subheadingTextColor};
`;