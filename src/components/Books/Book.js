import React, { useContext } from "react";
import styled from "styled-components";
import { BooksContext } from "../../context/BooksContext";

const BookStyle = styled.div`
  color: #fff;
  font-size: 1em;
  ${'' /* margin: 0 0 1em 0; */}
  ${'' /* margin: 0 0 0 1em; */}
  height: 350px;
  ${'' /* width: 100%; */}
  padding: 12px 12px;
  border-radius: 24px;
  background-image: url(${(props) => props.background || "palevioletred"});
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  &:hover {   
    cursor: pointer;
  }
  @media (max-width: 768px) {
    height: 250px;
    ${'' /* width: 140px; */}
    //margin: 0.25em 0;
    margin-top: 24px;
  }
`;

const ReadPercentage = styled.div`
  width: 24px;
  //opacity: 0.9;
  height: 18px;
  padding: 9px 18px;
  background: rgba(255, 255, 255, 0.1666);
  //box-shadow: 0 0 1rem 0 rgba(0, 0, 0, 0.2);
  text-align: center;
  position: absolute;
  ${'' /* top: 8px;
  left: 1px; */}
  font-size: 12px;
  font-weight: 600;
  //height: 20px;
  //box-shadow: 0 0 1rem 0 rgba(0, 0, 0, 0.2);
  border-radius: 12px;
  position: relative;
  overflow: hidden;
  display: flex;
  align-items: center;
`;
const Genre = styled.div`
  //background: #000;
  background: rgba(0, 0, 0, 0.6666);
  // height: 15px;
  padding: 9px 18px;
  border-radius: 12px;
  //border: 1px solid #000;
  position: absolute;
  bottom: 10px;
  right: 18px;
  font-size: 15px;
  text-align: center;
  font-weight: 600;
  color: #FFFFFF;
`;
export const Book = (props) => {
  const { modal, setModal, setBookInfo } = useContext(BooksContext);
  const x = {
    genre: props.genre,
    read_percentage: props.read_percentage,
    image: props.image,
    author: props.author,
    description: props.description,
    title: props.title,
  };
  return (
    <>
      <BookStyle
        background={props.image}
        onClick={() => {
          console.log(props.genre);
          setModal(!modal);
          setBookInfo(x);
        }}
      >
        {/* {props.title} */}
        <ReadPercentage>{props.read_percentage}%</ReadPercentage>
        <Genre>{props.genre}</Genre>
      </BookStyle>
    </>
  );
};
