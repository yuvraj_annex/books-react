import React, { useContext } from "react";
import { Book } from "./Book";
import styled from "styled-components";
import "./Books.css";
import { BooksContext } from "../../context/BooksContext";
export default function Books() {
  const { search } = useContext(BooksContext);

  return search.map((book, index) => {
    return (
      <div key={index} className={'book-wrapper'}>
        <Book key={index}
          image={book.image}
          isModalOpen={false}
          key={index}
          id={index}
          title={book.title}
          read_percentage={book.read_percentage}
          genre={book.genre}
          description={book.description}
          author={book.author}
        />

        <div className={'book-heading-section'}>
          <Heading>{book.title}</Heading>
          <SubHeading>{book.author}</SubHeading>
        </div>
      </div>
    );
  });
}

const styles = {
  book__details: {
    // marginLeft: "20px",
    textAlign: "center"
  },
};
const Heading = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  color: ${({ theme }) => theme.headingTextColor};;
  line-height: 24px;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;
const SubHeading = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;
  color: ${({ theme }) => theme.subheadingTextColor};;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;