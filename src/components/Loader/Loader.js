import React from "react";
import "./Loader.css";
const Loader = (props) => {
  // return <div>Loading...</div>
  return <div className={"loader-wrapper"}><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>;
}

export default Loader;