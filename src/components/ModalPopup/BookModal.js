import React, { useContext } from "react";
import Modal from "react-modal";
import styled from "styled-components";
import { BooksContext } from "../../context/BooksContext";
import { CloseIcon } from "./ModelIcon";

const CloseButton = styled.button`
  padding: 12px 25px;
  border-radius: 12px;
  background: #F9F9F9;
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;
  color: #3C3C3C;
  text-align: center;
  border: none;
  letter-spacing: -0.012em;
  &:hover {
    cursor: pointer;
  }
`;
const ContinueReading = styled.button`
  padding: 12px 87px;
  border-radius: 12px;
  background-color: #2478F0;
  border: none;
  margin-left: 18px;
  color: #fff;
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;
  @media (max-width: 768px) {
    padding: 12px 64px; 
  }
`;
const TopCloseIcon = styled.div`
  position: absolute;
  top: 30px;
  right: 30px;
  
  &:hover {
    cursor: pointer;
  }
  @media (max-width: 768px) {
    top: 18px;
    right: 18px;
  }
`;
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    transform: "translate(-50%, -50%)",
    border: "none",
  },
};
const Heading = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 30px;
  line-height: 36px;
  color: #1E1E1E;
  align-items: center;
  text-align: center;
  letter-spacing: -0.024em;
  margin-top: 42px;
  @media (max-width: 768px) {
    margin-top: 30px;
    display: -webkit-box;
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;
      overflow: hidden;
  }
`;
const Description = styled.div`
  margin-top: 18px;
  margin-bottom: 30px;
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;
  text-align: center;
  letter-spacing: -0.012em;
  color: #3C3C3C;
  @media (max-width: 768px) {
    display: -webkit-box;
    -webkit-line-clamp: 5;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }
`;
const SubTitle = styled.div`
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 24px;
    display: flex;
    align-items: center;
    text-align: center;
    letter-spacing: -0.015em;
    color: #696969;
    margin-top: 6px;
    text-align: center;
    justify-content: center;
    @media (max-width: 768px) {
      margin-top: 0;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;
      overflow: hidden;
     
    }
`;
const BookContainer = styled.div`
  max-width: 480px;
  //height: 500px;
  background: white;
  text-align: center;
  //padding: 30px;
  
  @media (max-width: 768px) {
    width: 100%;
    height: 100%;
    background: white;
    text-align: center;
    padding: 0px;
  }
`;
const BookModalContentWrapper = styled.div`
  font-size: 14px;
`;

const BookModal = (props) => {
  const { setModal, bookInfo } = useContext(BooksContext);
  const { image, title, author, description } = bookInfo;
  return (
    <Modal
      isOpen={true}
      ariaHideApp={false}
      // onAfterOpen={afterOpenModal}
      onRequestClose={() => setModal(false)}
      style={customStyles}
    >
      <TopCloseIcon onClick={() => setModal(false)}>
        <CloseIcon  />
      </TopCloseIcon>
      <BookContainer>
        <img src={image} className={"modal-img"} alt="" style={{ borderRadius: "24px" }} />
        <Heading>{title}</Heading>
        <SubTitle>{author}</SubTitle>
        <BookModalContentWrapper>  
        <Description>{description}</Description>
        
        </BookModalContentWrapper>

        <CloseButton onClick={() => setModal(false)}>Close</CloseButton>
        <ContinueReading onClick={() => setModal(false)}>
          Continue Reading
        </ContinueReading>
      </BookContainer>
    </Modal>
  );
};
export default BookModal;
